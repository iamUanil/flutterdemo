import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'HomeScreen.dart';

class RegisterScreen extends StatefulWidget {
  @override
  RegisterPageState createState() => new RegisterPageState();
}

class RegisterPageState extends State<RegisterScreen> {
  final GlobalKey<FormState> _registerFormKey = GlobalKey<FormState>();
  TextEditingController firstNameInputController;
  //TextEditingController lastNameInputController;
  TextEditingController emailInputController;
  TextEditingController passwordInputController;
  TextEditingController confirmPasswordController;

  @override
  void initState() {
    firstNameInputController = new TextEditingController();
   // lastNameInputController = new TextEditingController();
    emailInputController = new TextEditingController();
    passwordInputController = new TextEditingController();
    confirmPasswordController = TextEditingController();
    super.initState();
  }

  String emailValidator(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(value)) {
      return 'Email format is invalid';
    } else {
      return null;
    }
  }

  String pwdValidator(String value) {
    if (value.length < 8) {
      return 'Password must be longer than 8 characters';
    } else {
      return null;
    }
  }

  @override
  Widget build(BuildContext context) {
    final logo = Hero(
      tag: 'hero',
      child: CircleAvatar(
        backgroundColor: Colors.transparent,
        radius: 48.0,
        child: Image.asset('assets/logo.png'),
      ),
    );

    final firstName = TextFormField(
      autofocus: false,
      decoration: InputDecoration(
          hintText: 'User Name',
          contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 0.0),
          border:
              OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
      controller: firstNameInputController,
      // ignore: missing_return
      validator: (value) {
        if (value.length < 3) {
          return "Please enter a valid first name";
        }
      },
    );

   /* final lastName = TextFormField(
      autofocus: false,
      decoration: InputDecoration(
          hintText: 'Last Name',
          contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 0.0),
          border:
              OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
      controller: lastNameInputController,
      // ignore: missing_return
      validator: (value) {
        if (value.length < 3) {
          return "Please enter a valid last name";
        }
      },
    );*/

    final email = TextFormField(
      keyboardType: TextInputType.emailAddress,
      autofocus: false,
      decoration: InputDecoration(
          hintText: 'Email',
          contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 0.0),
          border:
              OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
      controller: emailInputController,
      validator: emailValidator,
    );

    final password = TextFormField(
      autofocus: false,
      decoration: InputDecoration(
          hintText: 'Password',
          contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 0.0),
          border:
              OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
      controller: passwordInputController,
      obscureText: true,
      validator: pwdValidator,
    );

    final cnfmPassword = TextFormField(
      autofocus: false,
      obscureText: true,
      decoration: InputDecoration(
          hintText: 'Confirm Password',
          contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 10.0, 20.0),
          border:
              OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
      controller: confirmPasswordController,
      validator: pwdValidator,
    );

    final registerButton = Padding(
      padding: EdgeInsets.symmetric(vertical: 16.0),
      child: RaisedButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(24),
        ),
        onPressed: () {
         // if(_registerFormKey.currentState.validate()){
            if(passwordInputController.text == confirmPasswordController.text){
              FirebaseAuth.instance.createUserWithEmailAndPassword(email: emailInputController.text, password: passwordInputController.text)
                  .then((currentUser) => Firestore.instance.collection("users").document(currentUser.user.uid)
                  .setData({"password": passwordInputController.text, //currentUser.user.uid
                            "username ": firstNameInputController.text,
                            "email": emailInputController.text})
                  .then((result) =>{
                      Navigator.push(context, MaterialPageRoute(
                          builder: (context) => Homescreen(text: firstNameInputController.text.toString(),text1: emailInputController.text.toString())),),
                /*builder: (context) => Homescreen(
                  title:
                  firstNameInputController
                      .text +
                      "'s Tasks",
                  uid: currentUser.uid,
                )*/
                firstNameInputController.clear(),
                emailInputController.clear(),
                passwordInputController.clear(),
                confirmPasswordController.clear()
              }).catchError((err) => {Scaffold.of(context).showSnackBar(SnackBar(content: Text(err.toString()),))})
                  .catchError((err) => {Scaffold.of(context).showSnackBar(SnackBar(content: Text(err.toString()),))
              })

              );
            }
         /* }else{
             showDialog(context: context,
             builder: (BuildContext context){
               return AlertDialog(
                 title: Text("Error"),
                 content: Text("The passwords do not mathc"),
                 actions: <Widget>[
                   FlatButton(
                     child: Text("Close"),
                     onPressed: (){ Navigator.of(context).pop();},
                   )
                 ],
               );
             });
          }*/
         /* Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => Homescreen()),
          );*/
        },
        padding: EdgeInsets.all(12),
        color: Colors.lightBlueAccent,
        child: Text('Register', style: TextStyle(color: Colors.white)),
      ),
    );

    final loginbtn = Padding(
      padding: EdgeInsets.symmetric(vertical: 16.0),
      child: FlatButton(
        child: Text("Login here!"),
        onPressed: () {
          Navigator.pop(context);
        },
      ),
    );

    return Scaffold(
      backgroundColor: Colors.white,
      key: _registerFormKey,
      body: Center(
        child: ListView(
          shrinkWrap: true,
          padding: EdgeInsets.only(left: 24.0, right: 24.0),
          children: <Widget>[
            logo,
            SizedBox(height: 48.0),
            firstName,
          /*  SizedBox(height: 12.0),
            lastName,*/
            SizedBox(height: 12.0),
            email,
            SizedBox(height: 12.0),
            password,
            SizedBox(height: 12.0),
            cnfmPassword,
            SizedBox(height: 12.0),
            registerButton,
            SizedBox(height: 12.0),
            loginbtn,
          ],
        ),
      ),
    );
  }
}