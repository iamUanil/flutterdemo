import 'dart:async';
import 'package:flutter/material.dart';
import 'package:hello_flutter/LoginScreen.dart';

void main() => runApp(new MaterialApp(
     home: new MySplashPage(),
     routes: <String,WidgetBuilder>{
       '/LoginScreen' : (BuildContext context) => new LoginScreen()
     },
));


class MySplashPage extends StatefulWidget {

  @override
  SplashScreen createState() => SplashScreen();
}

class SplashScreen extends State<MySplashPage> {

  startTime() async{
    var _duration = new Duration(seconds: 2);
    return new Timer(_duration,navigationPage);
  }

  void navigationPage(){
    Navigator.of(context).pushReplacementNamed("/LoginScreen");
  }

  @override
  void initState() {
    super.initState();
    startTime();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: Center(
        child: new Image.asset('assets/output_onlinepngtools.png')
      )
    );
  }
}