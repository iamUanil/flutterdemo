import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class SecondFragment extends StatefulWidget {
  @override
  SecondPage createState() => SecondPage();
}

class SecondPage extends State<SecondFragment> {
  Firestore firestore = Firestore.instance;
  List<DocumentSnapshot> users = [];
  bool isLoading = false;
  bool hasMore = true;
  int documentLimit = 10;
  DocumentSnapshot lastDocument;
  ScrollController _scrollController = ScrollController();

  @override
  void initState() {
    super.initState();
    getUsersList();
    _scrollController.addListener(() {
      double maxScroll = _scrollController.position.maxScrollExtent;
      double currentScroll = _scrollController.position.pixels;
      double delta = MediaQuery.of(context).size.height * 0.20;
      if (maxScroll - currentScroll <= delta) {
        getUsersList();
      }
    });
  }

  getUsersList() async {
    if (!hasMore) {
      print('No More users');
      return;
    }
    if (isLoading) {
      return;
    }
    setState(() {
      isLoading = true;
    });
    QuerySnapshot querySnapshot;
    if (lastDocument == null) {
      querySnapshot = await firestore
          .collection("users")
          .orderBy("email")
          .limit(documentLimit)
          .getDocuments();
    } else {
      querySnapshot = await firestore
          .collection("users")
          .orderBy("email")
          .startAfterDocument(lastDocument)
          .limit(documentLimit)
          .getDocuments();
      print(1);
    }
    if (querySnapshot.documents.length < documentLimit) {
      hasMore = false;
    }
    lastDocument = querySnapshot.documents[querySnapshot.documents.length - 1];
    users.addAll(querySnapshot.documents);
    setState(() {
      isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          Expanded(
            child: users.length == 0
                ? Center(
              child: Text('No Data...'),
            )
                : ListView.separated(
              controller: _scrollController,
              itemCount: users.length,
              itemBuilder: (context, index) {
                return Card(
                  child:  ListTile(
                    leading: CircleAvatar(
                      backgroundImage: AssetImage('assets/download.jpeg'),
                    ),
                    contentPadding: EdgeInsets.only(
                        left: 15, top: 5, right: 5, bottom: 5),
                    subtitle: Text(users[index].data["email"]),
                    title:
                    Text("Test Name"),
                    trailing: Icon(Icons.keyboard_arrow_right),//users[index].data["username"]
                  ),
                );
              },
              separatorBuilder: (context, index) {
                return Divider();
              },
            ),
          ),
          isLoading
              ? Container(
            width: MediaQuery.of(context).size.width,
            padding: EdgeInsets.all(5),
            color: Colors.yellowAccent,
            child: Text(
              'Loading',
              textAlign: TextAlign.center,
              style: TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
          )
              : Container()
        ],
      ),
    );
  }
}