import 'package:flutter/material.dart';
import 'fragment/FirstFragment.dart';
import 'fragment/SecondFragment.dart';
import 'fragment/ThirdFragment.dart';

class DrawerItem {
  String title;
  IconData icon;

  DrawerItem(this.title, this.icon);
}

class Homescreen extends StatefulWidget {
  String text;
  String text1;

  Homescreen({Key key, @required this.text, Key key1, @required this.text1})
      : super(key: key);

  //Homescreen({Key key, this.title, this.uid}) : super(key: key);

  final drawerItems = [
    new DrawerItem("HOME", Icons.home),
    new DrawerItem("WIFI", Icons.rss_feed),
    new DrawerItem("PIZZA",Icons.local_pizza),
    new DrawerItem("INFO", Icons.info)
  ];

  @override
  State<StatefulWidget> createState() {
    return new HomePageState();
  }
}

class HomePageState extends State<Homescreen> {
  int _selectedDrawerIndex = 0;

  _getDrawerItemWidget(int pos) {
    switch (pos) {
      case 0:
        return new FirstFragment();
      case 1:
        return new SecondFragment();
      case 2:
        return new ThirdFragment();
      case 3:
        return new ThirdFragment();
      default:
        return new Text("Error");
    }
  }

  _onSelectItem(int index) {
    setState(() => _selectedDrawerIndex = index);
    Navigator.of(context).pop(); // close the drawer
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> drawerOptions = [];
    for (var i = 0; i < widget.drawerItems.length; i++) {
      var d = widget.drawerItems[i];
      drawerOptions.add(new ListTile(
        leading: new Icon(d.icon),
        title: new Text(d.title),
        selected: i == _selectedDrawerIndex,
        onTap: () => _onSelectItem(i),
      ));
    }
    return new Scaffold(
      appBar: new AppBar(
        title: new Text(widget.drawerItems[_selectedDrawerIndex].title),
      ),
      drawer: new Drawer(
        child: new Column(
          children: <Widget>[
            UserAccountsDrawerHeader(
              decoration: BoxDecoration(color: Colors.white),
              margin: EdgeInsets.only(bottom: 40.0),
              currentAccountPicture: Container(
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    image: DecorationImage(
                        fit: BoxFit.fill,
                        image: NetworkImage("https://via.placeholder.com/150"))),
              ),
              accountName: new Container(
                  child: Text(widget.text,
                  style: TextStyle(color: Colors.black),
              )),
              accountEmail: new Container(
                  child: Text(widget.text1,
                  style: TextStyle(color: Colors.black),
              )),
            ),
            new Column(children: drawerOptions)
          ],
        ),
      ),
      body: _getDrawerItemWidget(_selectedDrawerIndex),
    );
  }
}